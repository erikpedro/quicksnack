package br.com.quicksnack.dto;

import java.io.Serializable;

import br.com.quicksnack.domain.Cardapio;

public class CardapioDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	private Integer id;
	private String nome;
	private String descricao;
	private Double preco;
	private String qrCode;

	
	public CardapioDTO() {
		
	}
	
	
public CardapioDTO(Cardapio obj) {
		id = obj.getId();
		nome = obj.getNome();
		preco = obj.getPreco();
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public Double getPreco() {
		return preco;
	}


	public void setPreco(Double preco) {
		this.preco = preco;
	}


	public String getQrCode() {
		return qrCode;
	}


	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	
	
	
	
	
	
	
}
