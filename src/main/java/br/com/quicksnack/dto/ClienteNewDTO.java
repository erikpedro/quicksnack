package br.com.quicksnack.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import br.com.quicksnack.services.validation.ClienteInsert;

@ClienteInsert
public class ClienteNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Preenchimento Obrigatorio")
	@Size(min = 03, max = 80, message ="O Tamanho deve ser entre 3 e 80 caracteres" )
	private String nome;
	
	@NotEmpty(message = "Preenchimento Obrigatorio")
	@Email(message = "Preenchimento Obrigatorio")
	private String email;
	
	@NotEmpty(message = "Preenchimento Obrigatorio")
	private String cpfOuCnpj;
	
	private Integer tipoCliente;

	private Integer messaId;
	
	private Integer numeroDaMesa;

	private String telefone1;
	private String telefone2;

	private Integer garconId;

	public ClienteNewDTO() {

	}

	public Integer getMessaId() {
		return messaId;
	}

	public void setMessaId(Integer messaId) {
		this.messaId = messaId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}

	public Integer getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(Integer tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public Integer getNumeroDaMesa() {
		return numeroDaMesa;
	}

	public void setNumeroDaMesa(Integer numeroDaMesa) {
		this.numeroDaMesa = numeroDaMesa;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public Integer getGarconId() {
		return garconId;
	}

	public void setGarconId(Integer garconId) {
		this.garconId = garconId;
	}

}
