package br.com.quicksnack.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.quicksnack.enums.EstadoPagamento;
import br.com.quicksnack.enums.TipoPagamentos;


@Entity
public class Pagamentos implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	private Integer id;
	
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private Date dataPagamento;
	
	
	private Integer estado;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name="pedido_id")
	@MapsId
	private Pedido pedido;
	
	private TipoPagamentos tipoPagamentos;
	
	public Pagamentos() {
		
	}
	
	public Pagamentos(Integer id, EstadoPagamento estado, Pedido pedido, TipoPagamentos tipoPagamentos, Date dataPagamento ) {
		super();
		this.id = id;
		this.estado = (estado == null) ? null : estado.getCod();
		this.pedido = pedido;
		this.tipoPagamentos = tipoPagamentos;
		this.dataPagamento = dataPagamento;
	}

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public EstadoPagamento getEstado() {
		return EstadoPagamento.toEnum(estado);
	}



	public void setEstado(EstadoPagamento estado) {
		this.estado = estado.getCod();
	}
	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public TipoPagamentos getTipoPagamentos() {
		return tipoPagamentos;
	}

	public void setTipoPagamentos(TipoPagamentos tipoPagamentos) {
		this.tipoPagamentos = tipoPagamentos;
	}
	
	
	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pagamentos other = (Pagamentos) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	
	
	
	
	

}
