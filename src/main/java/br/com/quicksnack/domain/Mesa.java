package br.com.quicksnack.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Mesa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer numeroDaMesa;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "garcon_id")
	private Garcon garcon;

	public Mesa() {

	}

	public Mesa(Integer id, Integer numeroDaMesa, Cliente cliente, Garcon garcon) {
		super();
		this.id = id;
		this.numeroDaMesa = numeroDaMesa;
		this.cliente = cliente;
		this.setGarcon(garcon);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumeroDaMesa() {
		return numeroDaMesa;
	}

	public void setNumeroDaMesa(Integer numeroDaMesa) {
		this.numeroDaMesa = numeroDaMesa;
	}

	public Garcon getGarcon() {
		return garcon;
	}

	public void setGarcon(Garcon garcon) {
		this.garcon = garcon;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesa other = (Mesa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
