package br.com.quicksnack.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Garcon implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nome;
	private String avaliacao;
	private String elogios;
	private String reclamacoes;
	
	@ManyToOne
	@JoinColumn(name="tipo_id")
	private Tipo tipo;

	
	public Garcon() {
	}

	public Garcon(Integer id, String nome, String avaliacao, String elogios, String reclamacoes, Tipo tipo) {
		super();
		this.id = id;
		this.nome = nome;
		this.avaliacao = avaliacao;
		this.elogios = elogios;
		this.reclamacoes = reclamacoes;
		this.tipo = tipo;
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAvaliacao() {
		return avaliacao;
	}

	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}

	public String getReclamacoes() {
		return reclamacoes;
	}

	public void setReclamacoes(String reclamacoes) {
		this.reclamacoes = reclamacoes;
	}

	public String getElogios() {
		return elogios;
	}

	public void setElogios(String elogios) {
		this.elogios = elogios;
	}
	
	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Garcon other = (Garcon) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
