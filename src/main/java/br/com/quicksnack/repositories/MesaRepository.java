package br.com.quicksnack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.quicksnack.domain.Mesa;

@Repository
public interface MesaRepository extends JpaRepository<Mesa, Integer> {

}
