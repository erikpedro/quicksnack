package br.com.quicksnack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.quicksnack.domain.Pagamentos;

@Repository
public interface PagamentosRepository extends JpaRepository<Pagamentos, Integer> {

}
