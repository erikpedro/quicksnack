package br.com.quicksnack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.quicksnack.domain.Garcon;

@Repository
public interface GarconRepository extends JpaRepository<Garcon, Integer> {

}
