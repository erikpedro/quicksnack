package br.com.quicksnack.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.quicksnack.domain.Cardapio;
import br.com.quicksnack.dto.CardapioDTO;
import br.com.quicksnack.resources.utils.URL;
import br.com.quicksnack.services.CardapioService;

@RestController
@RequestMapping(value="/cardapio")
public class CardapioResource {
	
	@Autowired
	private CardapioService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Cardapio> find(@PathVariable Integer id) {
		Cardapio obj = service.find(id);
		return ResponseEntity.ok().body(obj);
		
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Page<CardapioDTO>> findPage(
			@RequestParam(value = "nome", defaultValue = "")  String nome,
			@RequestParam(value = "categoria", defaultValue = "")  String categoria, 
			@RequestParam(value = "page", defaultValue = "0")  Integer page, 
			@RequestParam(value = "linesPerPage", defaultValue = "24")  Integer linesPerPage, 
			@RequestParam(value = "orderBy", defaultValue = "nome")  String orderBy, 
			@RequestParam(value = "direction", defaultValue = "ASC")  String direction) {
		String nomeDecoded = URL.decodeParam(nome);
		
		List<Integer> ids = URL.decodeIntList(categoria);
	 Page<Cardapio> list = service.search(nomeDecoded, ids, page, linesPerPage, orderBy, direction);
	 Page<CardapioDTO> listDto = list.map(obj -> new CardapioDTO(obj));
		return ResponseEntity.ok().body(listDto);
		
	}
	

}
