package br.com.quicksnack.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.quicksnack.services.DbService;

@Configuration
@Profile("test")
public class TesteConfig {

	@Autowired
	private DbService dbService;

	
	@Bean
	public boolean dbh2call() throws ParseException {
		dbService.instatieteTesteh2d();
		return true;
	}

}
