package br.com.quicksnack.enums;

public enum TipoPagamentos {
	DEBITO(1, "Cartao de Debito"),
	CREDITO(2, "Cartao de Credito"),
	DINHEIRO(3, "Pagamento Com Dinheiro");
	
	
	
	
	private Integer cod;
	private String descricao;
	
	private TipoPagamentos(Integer cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	
	
	public Integer getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	
	
public static TipoPagamentos  toEnum(Integer cod) {
	if (cod == null) {
		return null;
	}
	
	for (TipoPagamentos tc : TipoPagamentos.values()) {
		if(cod.equals(tc.getCod())) {
			return tc;
		}
		
	}
	
	throw new IllegalArgumentException("Id invalido: " + cod);
	
}

}
