package br.com.quicksnack.enums;

public enum EstadoPagamento {
	PAGO(1, "Conta Paga"),
	ABERTO(2, "Conta em Aberto"),
	CANCELADO(3, "Conta Cancelada");
	
	
	
	
	private Integer cod;
	private String descricao;
	
	private EstadoPagamento(Integer cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	
	
	public Integer getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	
	
public static EstadoPagamento  toEnum(Integer cod) {
	if (cod == null) {
		return null;
	}
	
	for (EstadoPagamento tc : EstadoPagamento.values()) {
		if(cod.equals(tc.getCod())) {
			return tc;
		}
		
	}
	
	throw new IllegalArgumentException("Id invalido: " + cod);
	
}
	
	
	
	
}
