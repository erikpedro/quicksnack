package br.com.quicksnack.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.quicksnack.domain.ItemPedido;
import br.com.quicksnack.domain.Pedido;
import br.com.quicksnack.enums.EstadoPagamento;
import br.com.quicksnack.repositories.ItemPedidoRepository;
import br.com.quicksnack.repositories.PagamentosRepository;
import br.com.quicksnack.repositories.PedidoRepository;
import br.com.quicksnack.services.exceptions.ObjectNotFoundException;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository repo;

	@Autowired
	private PagamentosRepository pagamentoRepository;

	@Autowired
	private ItemPedidoRepository itemPedidoRepository;

	@Autowired
	private CardapioService cardapioService;

	public Pedido find(Integer id) {
		Optional<Pedido> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getName()));

	}

	public Pedido insert(Pedido obj) {
		obj.setId(null);
		obj.setInstante(new Date());
		obj.getPagamentos().setEstado(EstadoPagamento.ABERTO);
		obj.getPagamentos().setPedido(obj);
		obj = repo.save(obj);
		pagamentoRepository.save(obj.getPagamentos());
		for (ItemPedido ip : obj.getItens()) {
			ip.setDesconto(0.0);
			ip.setPreco(cardapioService.find(ip.getCardapio().getId()).getPreco());
			ip.setPedido(obj);
		}
		itemPedidoRepository.saveAll(obj.getItens());
		return obj;
	}

}
