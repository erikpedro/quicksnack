package br.com.quicksnack.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.quicksnack.domain.Cardapio;
import br.com.quicksnack.domain.Categoria;
import br.com.quicksnack.domain.Cliente;
import br.com.quicksnack.domain.Garcon;
import br.com.quicksnack.domain.ItemPedido;
import br.com.quicksnack.domain.Mesa;
import br.com.quicksnack.domain.Pagamentos;
import br.com.quicksnack.domain.Pedido;
import br.com.quicksnack.domain.Tipo;
import br.com.quicksnack.domain.enums.TipoCliente;
import br.com.quicksnack.enums.EstadoPagamento;
import br.com.quicksnack.enums.TipoPagamentos;
import br.com.quicksnack.repositories.CardapioRepository;
import br.com.quicksnack.repositories.CategoriaRepository;
import br.com.quicksnack.repositories.ClienteRepository;
import br.com.quicksnack.repositories.GarconRepository;
import br.com.quicksnack.repositories.ItemPedidoRepository;
import br.com.quicksnack.repositories.MesaRepository;
import br.com.quicksnack.repositories.PagamentosRepository;
import br.com.quicksnack.repositories.PedidoRepository;
import br.com.quicksnack.repositories.TipoRepository;

@Service
public class DbService {

	@Autowired
	private CategoriaRepository categoriaRepository;

	@Autowired
	private CardapioRepository cardapioRepository;

	@Autowired
	private GarconRepository garconRepository;

	@Autowired
	private TipoRepository tipoRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private MesaRepository mesaRepository;

	@Autowired
	private PedidoRepository pedidoRepository;

	@Autowired
	private PagamentosRepository pagamentosRepository;

	@Autowired
	private ItemPedidoRepository itemPedidoRepository;

	public void instatieteTesteh2d() throws ParseException {
		
		Categoria cat1 = new Categoria(null, "Petiscos");
		Categoria cat2 = new Categoria(null, "Comidas");
		Categoria cat3 = new Categoria(null, "Bebidas");
		Categoria cat4 = new Categoria(null, "Sobremesas");
		Categoria cat5 = new Categoria(null, "cat5");
		Categoria cat6 = new Categoria(null, "cat6");
		Categoria cat7 = new Categoria(null, "cat7");
		Categoria cat8 = new Categoria(null, "cat8");
		Categoria cat9 = new Categoria(null, "cat9");
		Categoria cat10 = new Categoria(null, "cat10");

		Cardapio menu1 = new Cardapio(null, "Carne De Sol", "Carne De Sol Com Mandioca", 50.0, "123456");
		Cardapio menu2 = new Cardapio(null, "Jantinho", "Carne De Sol arroz e feijao", 10.0, "abcde");
		Cardapio menu3 = new Cardapio(null, "Coca-Cola", "Refrigerante", 5.0, "1a2b3a5g");
		Cardapio menu4 = new Cardapio(null, "Sorvete", "Flocos", 5.0, "1a2d3f4g5h6");
		Cardapio menu5 = new Cardapio(null, "Amistel", "Cerveja 600 ml", 8.0, "1a2b3a5g");
		Cardapio menu6 = new Cardapio(null, "Chocolate", "Trufado", 5.0, "1a2d3f4g5h6");

		cat1.getCardapios().add(menu1);
		cat2.getCardapios().add(menu2);
		cat3.getCardapios().addAll(Arrays.asList(menu3, menu5));
		cat4.getCardapios().addAll(Arrays.asList(menu4, menu6));

		menu1.getCategorias().addAll(Arrays.asList(cat1, cat2));
		menu2.getCategorias().addAll(Arrays.asList(cat2));
		menu3.getCategorias().addAll(Arrays.asList(cat3));
		menu4.getCategorias().addAll(Arrays.asList(cat4));
		menu5.getCategorias().addAll(Arrays.asList(cat3));
		menu6.getCategorias().addAll(Arrays.asList(cat4));

		categoriaRepository.saveAll(Arrays.asList(cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, cat9, cat10));
		cardapioRepository.saveAll(Arrays.asList(menu1, menu2, menu3, menu4, menu5, menu6));

		Tipo freela = new Tipo(null, "Freela");
		Tipo Contrato = new Tipo(null, "Contrato");
		Tipo CLT = new Tipo(null, "CLT");

		Garcon gar = new Garcon(null, "Rafael", "5", "Pega Jordan", "", freela);
		Garcon gar1 = new Garcon(null, "Fofim", "5", "Bebim", "", Contrato);
		Garcon gar2 = new Garcon(null, "Luquinha", "5", "Porcelana", "Baitola", CLT);

		freela.getGarcons().addAll(Arrays.asList(gar2));
		Contrato.getGarcons().addAll(Arrays.asList(gar1));
		CLT.getGarcons().addAll(Arrays.asList(gar));

		tipoRepository.saveAll(Arrays.asList(freela, Contrato, CLT));
		garconRepository.saveAll(Arrays.asList(gar, gar1, gar2));

		Cliente cli1 = new Cliente(null, "Luquinha", "Luquinha@porcelana.com", "000.000.000.71",
				TipoCliente.PESSOAFISICA);
		Cliente cli2 = new Cliente(null, "Erik", "erik.pedro.com", "000.000.000.72", TipoCliente.PESSOAJURIDICA);
		Cliente cli3 = new Cliente(null, "Pedro", "pedro.com", "000.000.000.73", TipoCliente.CLIENTEAPP);
		cli1.getTelefones().addAll(Arrays.asList("123", "456"));
		cli2.getTelefones().addAll(Arrays.asList("13", "23"));
		cli3.getTelefones().addAll(Arrays.asList("23", "12"));

		Mesa m1 = new Mesa(null, 1, cli1, gar);
		Mesa m2 = new Mesa(null, 2, cli2, gar1);
		Mesa m3 = new Mesa(null, 3, cli3, gar2);

		cli1.getMesas().addAll(Arrays.asList(m1));
		cli2.getMesas().addAll(Arrays.asList(m2));
		cli3.getMesas().addAll(Arrays.asList(m3));

		clienteRepository.saveAll(Arrays.asList(cli1, cli2, cli3));
		mesaRepository.saveAll(Arrays.asList(m1, m2, m3));

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		Pedido ped1 = new Pedido(null, sdf.parse("25/03/2019 23:50"), cli1, m1);
		Pedido ped2 = new Pedido(null, sdf.parse("25/03/2019 23:50"), cli2, m2);

		Pagamentos pag1 = new Pagamentos(null, EstadoPagamento.PAGO, ped1, TipoPagamentos.CREDITO,
				sdf.parse("26/03/2019 01:50"));
		ped1.setPagamentos(pag1);

		Pagamentos pag2 = new Pagamentos(null, EstadoPagamento.ABERTO, ped2, TipoPagamentos.DINHEIRO,
				sdf.parse("26/03/2019 02:50"));
		ped2.setPagamentos(pag2);

		cli1.getPedidos().addAll(Arrays.asList(ped1));
		cli2.getPedidos().addAll(Arrays.asList(ped2));

		pedidoRepository.saveAll(Arrays.asList(ped1, ped2));
		pagamentosRepository.saveAll(Arrays.asList(pag1, pag2));

		ItemPedido ip1 = new ItemPedido(ped1, menu2, 0.00, 1, 50.0);
		ItemPedido ip2 = new ItemPedido(ped1, menu3, 0.00, 2, 5.0);
		ItemPedido ip3 = new ItemPedido(ped2, menu1, 0.00, 1, 55.0);

		ped1.getItens().addAll(Arrays.asList(ip1, ip2));
		ped2.getItens().addAll(Arrays.asList(ip3));

		menu2.getItens().addAll(Arrays.asList(ip1));
		menu3.getItens().addAll(Arrays.asList(ip2));
		menu1.getItens().addAll(Arrays.asList(ip3));

		itemPedidoRepository.saveAll(Arrays.asList(ip1, ip2, ip3));
	}

}
