package br.com.quicksnack.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.quicksnack.domain.Cardapio;
import br.com.quicksnack.domain.Categoria;
import br.com.quicksnack.repositories.CardapioRepository;
import br.com.quicksnack.repositories.CategoriaRepository;
import br.com.quicksnack.services.exceptions.ObjectNotFoundException;

@Service
public class CardapioService {
	
	@Autowired
	private CardapioRepository repo;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public Cardapio find(Integer id) {
		Optional<Cardapio> obj = repo.findById(id);
		return obj.orElseThrow(() 
				-> new ObjectNotFoundException
				("Objeto não encontrado! Id: " + id + ", Tipo: "  + Cardapio.class.getName()));
		
	}
	
	public Page<Cardapio> search(String nome, List<Integer> ids, Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		List<Categoria> categorias = categoriaRepository.findAllById(ids);
		return repo.search(nome, categorias, pageRequest);
		
	}
	

}
